#!/usr/bin/env python3
# -*- coding: utf-8 -*-


# Se crea una funcion la cual dara la posicion inicial
def NumeroAlAzar(tam):
    import random
    Num = int(random.randrange(tam))
    return Num


# Aca estara toda la base del programa
def ProgramaInicial(N):
    cambio = []
    max = N
    fin = False
    min = 0
    tam = N*N
    matriz = []
    ver = None
    gen = 1
    quintagenviva = 0
    # Se crea una lista, llenandola con "-"
    for x in range(0, tam):
        matriz.append('-')
        pass
    # Se creara la posicion inicial de la "celula"
    celini = NumeroAlAzar(tam)
    matriz[celini] = 'X'
    # Alrededor de la celula se crearan 4 vecinas
    for x in range(0, tam):
        if matriz[x] == 'X':
            if celini >= N:
                # Arriba
                matriz[x-N] = 'x'
                pass
            if celini < tam-N:
                # Abajo
                matriz[x+N] = 'x'
                pass
            if celini != 0 and celini % N != 0 and celini > N-1:
                # Arriba-izquierda
                matriz[x-1-N] = 'x'
                pass
            if celini % N != N-1:
                # Derecha
                matriz[x+1] = 'x'
                pass
            # Se reemplaza mayuscula con minuscula
            matriz[x] = 'x'
            pass
        pass
    # Se crea una segunda matriz igual a la original
    for x in range(0, tam):
        cambio.append(matriz[x])
        pass
    # Se crea el ciclo
    while fin is not True:
        # Se actualiza la matriz con los cambios
        for x in range(0, tam):
            matriz[x] = cambio[x]
            pass
        print("Generacion: ", gen)
        print("Celulas vivas: ", matriz.count('x'))
        print("Celulas muertas: ", matriz.count('-'))
        minrep = min
        maxrep = max
        # Se muestra la matriz, en NxN
        for x in range(0, N):
            print(matriz[minrep:maxrep])
            maxrep = maxrep+N
            minrep = minrep+N
            pass
        print("\n")
        import time
        time.sleep(1)
        # La posicion de la celula que se investigara
        for x in range(0, tam):
            ver = 0
            # se observara alrededor de la celula con scan
            for scn in range(-1, 2):
                # En el caso que este muerta
                if matriz[x] == '-':
                    # Si la celula esta en la esquina izquierda-superior
                    if x == 0:
                        if matriz[1] == 'x':
                            if matriz[N] == 'x':
                                if matriz[N+1] == 'x':
                                    matriz[x] = 'x'
                                    pass
                                pass
                            pass
                        pass
                    # Si la celula esta en la esquina izquierda-inferior
                    elif x == tam-N:
                        if matriz[x+1] == 'x':
                            if matriz[x-N] == 'x':
                                if matriz[x-N+1] == 'x':
                                    matriz[x] == 'x'
                                    pass
                                pass
                            pass
                        pass
                    # Si la celula esta en la esquina derecha-superior
                    elif x == N-1:
                        if matriz[x-1] == 'x':
                            if matriz[x+N] == 'x':
                                if matriz[x+N-1] == 'x':
                                    matriz[x] = 'x'
                                    pass
                                pass
                            pass
                        pass
                    # Si la celula esta en la esquina derecha-inferior
                    elif x == tam-1:
                        if matriz[x-1] == 'x':
                            if matriz[x-N] == 'x':
                                if matriz[x-N-1] == 'x':
                                    matriz[x] = 'x'
                                    pass
                                pass
                            pass
                        pass
                    # Si la celula esta en la primera fila
                    elif x < N:
                        if matriz[x+scn] == 'x' and x+scn != x:
                            ver = ver+1
                            pass
                        if matriz[x+scn+N] == 'x':
                            ver = ver+1
                            pass
                        pass
                    # Si la celula esta en la ultima fila
                    elif x >= tam-N:
                        if matriz[x+scn] == 'x' and x+scn != x:
                            ver = ver+1
                            pass
                        if matriz[x+scn-N] == 'x':
                            ver = ver+1
                            pass
                        pass
                    # Si la celula esta en la primera columna
                    elif x % N == 0:
                        if matriz[x+scn-N] == 'x' and scn != -1:
                            ver = ver+1
                            pass
                        if matriz[x+scn] == 'x' and scn != -1 and x+scn != x:
                            ver = ver+1
                            pass
                        if matriz[x+scn+N] == 'x' and scn != -1:
                            ver = ver+1
                            pass
                        pass
                    # Si la celula esta en la ultima columna
                    elif x % N == N-1:
                        if matriz[x+scn-N] == 'x' and scn != 1:
                            ver = ver+1
                            pass
                        if matriz[x+scn] == 'x' and scn != 1 and x+scn != x:
                            ver = ver+1
                            pass
                        if scn != 1:
                            if matriz[x+scn+N] == 'x':
                                ver = ver+1
                                pass
                            pass
                        pass
                    # Si la celula no se presente en algun borde
                    else:
                        if matriz[x+scn-N] == 'x':
                            ver = ver+1
                            pass
                        if matriz[x+scn] == 'x' and x+scn != x:
                            ver = ver+1
                            pass
                        if matriz[x+scn+N] == 'x':
                            ver = ver+1
                            pass
                        pass
                    # Si la celula muerta detecto 3 vivas, revive
                    if ver == 3:
                        cambio[x] = 'x'
                        pass
                    pass
                # En el caso que la celula este viva
                if matriz[x] == 'x':
                    # Si la celula esta en la esquina izquierda-superior
                    if x == 0:
                        if scn != -1:
                            if matriz[x+scn] == 'x' and scn+x != x:
                                ver = ver+1
                                pass
                            if matriz[x+scn+N] == 'x':
                                ver = ver+1
                                pass
                            pass
                        pass
                    # Si la celula esta en la esquina izquierda-inferior
                    elif x == tam-N:
                        if scn != -1:
                            if matriz[x+scn] == 'x' and x+scn != x:
                                ver = ver+1
                                pass
                            if matriz[x+scn-N] == 'x':
                                ver = ver+1
                                pass
                            pass
                        pass
                    # Si la celula esta en la esquina derecha-superior
                    elif x == N-1:
                        if scn != 1:
                            if matriz[x+scn] == 'x' and x+scn != x:
                                ver = ver+1
                                pass
                            if matriz[x+scn+N] == 'x':
                                ver = ver+1
                                pass
                            pass
                        pass
                    # Si la celula esta en la esquina derecha-inferior
                    elif x == tam-1:
                        if scn != 1:
                            if matriz[x+scn] == 'x' and x+scn != x:
                                ver = ver+1
                                pass
                            if matriz[x+scn-N] == 'x':
                                ver = ver+1
                                pass
                            pass
                        pass
                    # Si la celula esta en la primera fila
                    elif x < N:
                        if matriz[x+scn] == 'x' and x+scn != x:
                            ver = ver+1
                            pass
                        if matriz[x+scn+N] == 'x':
                            ver = ver+1
                            pass
                        pass
                    # Si la celula esta en la ultima fila
                    elif x >= tam-N:
                        if matriz[x+scn] == 'x' and x+scn != x:
                            ver = ver+1
                            pass
                        if matriz[x+scn-N] == 'x':
                            ver = ver+1
                            pass
                        pass
                    # Si la celula esta en la primera columna
                    elif x % N == 0:
                        if scn != -1:
                            if matriz[x+scn-N] == 'x':
                                ver = ver+1
                                pass
                            if matriz[x+scn] == 'x' and x+scn != x:
                                ver = ver+1
                                pass
                            if matriz[x+scn+N] == 'x':
                                ver = ver+1
                                pass
                            pass
                        pass
                    # Si la celula esta en la ultima columna
                    elif x % N == N-1:
                        if scn != 1:
                            if matriz[x+scn-N] == 'x':
                                ver = ver+1
                                pass
                            if matriz[x+scn] == 'x' and x+scn != x:
                                ver = ver+1
                                pass
                            if matriz[x+scn+N] == 'x':
                                ver = ver+1
                                pass
                            pass
                        pass
                    # Si la celula no se presente en algun borde
                    else:
                        if matriz[x+scn-N] == 'x':
                            ver = ver+1
                            pass
                        if matriz[x+scn] == 'x' and x+scn != x:
                            ver = ver+1
                            pass
                        if matriz[x+scn+N] == 'x':
                            ver = ver+1
                            pass
                        pass
                    # El caso de que la celula viva pase a muerta
                    if ver <= 1 or ver >= 4:
                        cambio[x] = '-'
                        pass
                    # En el caso de que la celula se mantenga viva
                    elif ver > 1 and ver < 4:
                        cambio[x] = 'x'
                        pass
                    pass
                pass
            pass
        gen = gen+1
        # Cada quinta generacion, se guarda las vivas
        if gen % 5 == 0:
            quintagenviva = cambio.count('x')
            pass
        # Caso de fin 1: Todas muertas
        if matriz.count('x') == 0:
            print("Todas las celulas estan muertas")
            fin = True
            pass
        # Caso de fin 2: Ciclo sin fin
        elif quintagenviva == matriz.count('x'):
            if matriz.count('x') == cambio.count('x'):
                if gen % 5 == 3:
                    fin = True
                    print("Se encontro un ciclo")
                    pass
                pass
            pass
    print("Juego terminado")
    pass


# El programa inicial donde se consigue el tamaño
try:
    N = int(input("de que tamaño sera su matriz NxN: "))
    ProgramaInicial(N)
except ValueError:
    print("El valor ingresado no es correcto")
